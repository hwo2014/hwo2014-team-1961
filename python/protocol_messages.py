
class SwitchLane(object):
    LEFT = 'Left'
    RIGHT = 'Right'


class Car(object):
    """A car class.

    >>> name = 'Rosberg'
    >>> color = 'blue'
    >>> c = Car({'name': name, 'color': color})
    >>> c.name
    'Rosberg'
    >>> c.color
    'blue'
    >>> c.to_dict() == {'name': name, 'color': color}
    True
    """

    def __init__(self, d):
        self.__name = d.get('name', '')
        self.__color = d.get('color', '')
    
    @property
    def name(self):
        return self.__name

    @property
    def color(self):
        return self.__color

    def __eq__(self, other):
        """Overriding equality check.

        >>> a = Car({'name': '1', 'color': '1'})
        >>> b = Car({'name': '1', 'color': '1'})
        >>> c = Car({'name': '2', 'color': '2'})
        >>>
        >>> a == b
        True
        >>> a == c
        False
        """
        return self.__dict__ == other.__dict__
    
    def to_dict(self):
        return {'name': self.name,
                'color': self.color
                }


class Message(object):
    """A basic message type.
    
    >>> a = Message('test_msg')
    >>> a.msg_type
    'test_msg'
    >>> a.to_protocol_dict() == {'msgType': 'test_msg'}
    True
    """

    def __init__(self, msg_type):
        self.__msg_type = msg_type

    @property
    def msg_type(self):
        return self.__msg_type
    
    def to_protocol_dict(self):
        """Creates a dictionary from the current Message instance."""
        return {'msgType': self.msg_type}


class MessagePing(Message):
    """A "ping" message, inherits from Message class.

    >>> a = MessagePing()
    >>> a.msg_type
    'ping'
    >>> a.to_protocol_dict() == {'msgType': 'ping'}
    True
    """

    MSG_TYPE = 'ping'
    
    def __init__(self):
        super(MessagePing, self).__init__(self.MSG_TYPE)


class MessageSwitchLane(Message):
    """A "switchLane" message, inherits from Message class.

    >>> switchLane = SwitchLane.LEFT
    >>> a = MessageSwitchLane(switchLane)
    >>> a.msg_type
    'switchLane'
    >>> a.switch_lane
    'Left'
    >>> a.to_protocol_dict() == {'msgType': 'switchLane', \
                                 'data': switchLane}
    True
    """

    MSG_TYPE = 'switchLane'
    
    def __init__(self, switch_lane):
        super(MessageSwitchLane, self).__init__(self.MSG_TYPE)
        self.__switch_lane = switch_lane
    
    @property
    def switch_lane(self):
        return self.__switch_lane
    
    def to_protocol_dict(self):
        d = super(MessageSwitchLane, self).to_protocol_dict()
        d['data'] = self.switch_lane

        return d


class MessageJoin(Message):
    """A "join" message, inherits from Message class.

    >>> a = MessageJoin('name', 'key')
    >>> a.msg_type
    'join'
    >>> a.name
    'name'
    >>> a.key
    'key'
    >>> a.to_protocol_dict() == {'msgType': 'join', 'data': {'name': 'name', 'key': 'key'}}
    True
    """

    MSG_TYPE = 'join'
    
    def __init__(self, name, key):
        super(MessageJoin, self).__init__(self.MSG_TYPE)

        self.__name = name
        self.__key = key

    @property
    def name(self):
        return self.__name
    
    @property
    def key(self):
        return self.__key
    
    def to_protocol_dict(self):
        d = super(MessageJoin, self).to_protocol_dict()
        d['data'] = {
            'key': self.key,
            'name': self.name
        }

        return d


class MessageThrottle(Message):
    """A "throttle" message, inherits from Message class.

    >>> a = MessageThrottle(0.5)
    >>> a.msg_type
    'throttle'
    >>> a.throttle
    0.5
    >>> a.to_protocol_dict() == {'msgType': 'throttle', 'data': 0.5}
    True
    """

    MSG_TYPE = 'throttle'
    
    def __init__(self, throttle):
        super(MessageThrottle, self).__init__(self.MSG_TYPE)

        self.__throttle = throttle

    @property
    def throttle(self):
        return self.__throttle

    def to_protocol_dict(self):
        d = super(MessageThrottle, self).to_protocol_dict()
        d['data'] = self.throttle

        return d

        
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
