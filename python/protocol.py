from eventhook import EventHook
from protocol_messages import MessageJoin, MessagePing

import json

class MessageHandler(object):

    def __init__(self, server_connection, name, key):
        self.server = server_connection
        self.server.on_receive += self.handle_msg

        self.__name = name
        self.__key = key
        
        self.on_yourcar = EventHook()
        self.on_gameinit = EventHook()
        self.on_carpositions = EventHook()
        self.on_crash = EventHook()
        self.on_lapfinished = EventHook()
        self.on_turboavailable = EventHook()
        
        self.__events_map = {
            'yourCar': self.on_yourcar,
            'gameInit': self.on_gameinit,
            'carPositions': self.on_carpositions,
            'crash': self.on_crash,
            'lapFinished' : self.on_lapfinished,
            'turboAvailable': self.on_turboavailable
        }

    @property
    def name(self):
        return self.__name
    
    @property
    def key(self):
        return self.__key

    def __send_join(self):
        """Constructs and sends the join message to the server."""
        self.send(MessageJoin(self.name, self.key))
        
    def send(self, protocol_message):
        self.server.send(json.dumps(protocol_message.to_protocol_dict()))

    def ping(self):
        """Sends a ping message to the server."""
        self.send(MessagePing())
        
    def handle_msg(self, msg):
        ignore_msg = False
        try:
            json_msg = json.loads(msg)

            msg_type = json_msg['msgType']
            msg_data = json_msg['data']

            ignore_msg = not self.__events_map.has_key(msg_type)
        except ValueError:
            ignore_msg = True

        if ignore_msg:
            print('Skipping message: %s' % msg_type)
            self.ping()
        else:
            self.__events_map[msg_type].fire(msg_data)
        
    def run(self):
        self.__send_join()
        self.server.run()
        
