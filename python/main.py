from ai import Bot1Ch
from protocol import MessageHandler
from connection import Connection

import sys

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        server_connection = Connection()
        server_connection.connect((host, int(port)))
        message_handler = MessageHandler(server_connection, name, key)
        bot = Bot1Ch(message_handler)
        bot.run()
