from circuit import Circuit

from search import sort_lane_paths
from protocol import MessageHandler
from protocol_messages import Car, MessageThrottle, MessageSwitchLane, SwitchLane
from pid import PID

import math

class Bot1Ch(object):

    def __init__(self, message_handler):
        self.message_handler = message_handler
        self.__car = None
        self.__circuit = None
        self.__paths = None
        self.__last_switchlane = -1
        
        # Register for different message events.
        self.message_handler.on_yourcar += self.on_yourcar
        self.message_handler.on_gameinit += self.on_gameinit
        self.message_handler.on_carpositions += self.on_carpositions
        self.message_handler.on_crash += self.on_crash
        self.message_handler.on_lapfinished += self.on_lapfinished
        self.message_handler.on_turboavailable += self.on_turboavailable

        # Create PID and set target speed (params can be optimised)
        self.pid = PID(5.0,0.4,1.2)
        self.pid.setPoint(8.8) 

        self.speed = 0
        self.last_index = 0
        self.last_distance = 0.0

    @property
    def car(self):
        return self.__car

    @property
    def circuit(self):
        return self.__circuit

    @property
    def paths(self):
        """Don't forget that this is a list of the paths sorted by length!!!

        Which means if you want the fastest route you need to get the first
        element (i.e self.paths[0])."""        
        return self.__paths
    
    def send_throttle(self, throttle):
        """Sends a throttle message."""
        self.message_handler.send(MessageThrottle(throttle))

    def send_switchlane(self, lane):
        """Sends a switch lane message."""
        print('Sending switch lane %s' % lane)
        self.message_handler.send(MessageSwitchLane(lane))
    
    def ping(self):
        """Sends a ping message."""
        self.message_handler.ping()

    def run(self):
        self.message_handler.run()

    def on_yourcar(self, data):
        """A message that identifies the current bot car on the race."""
        print('Received yourCar.')
        self.__car = Car(data)

    def on_gameinit(self, data):
        """A message that describes the current track."""
        self.__circuit = Circuit.from_dict(data['race']['track'])
        self.__paths = sort_lane_paths(self.__circuit)

        # Simple target speed based on angle of current and neighbouring pieces 
        pieces =  self.__circuit.pieces
        num_pieces = len(pieces)
        self.targetSpeed = num_pieces * [8.8]
        for i in range(0,num_pieces):
            if pieces[i].angle() != 0:
                self.targetSpeed[i] -= 0.9
            if pieces[(i-1) % num_pieces].angle() != 0:
                self.targetSpeed[i] -= 0.9
            if pieces[(i+1) % num_pieces].angle() != 0:
                self.targetSpeed[i] -= 0.9
    
    def on_carpositions(self, data):
        """A message that describes the car positions, sent on every game tick."""

        car_position = Bot1Ch.__retrieve_self_carposition_data(self.car, data)

        piece_position = car_position['piecePosition']
        piece_index = piece_position['pieceIndex']
        piece_dist = piece_position["inPieceDistance"]

        # Lane switching
        next_switchlane = Bot1Ch.__next_switchlane(self.circuit.pieces, piece_index)

        if self.__last_switchlane != next_switchlane and self.paths[0][next_switchlane] != piece_position['lane']['endLaneIndex']:
            self.__last_switchlane = next_switchlane

            if self.paths[0][next_switchlane] > piece_position['lane']['endLaneIndex']:
                self.send_switchlane(SwitchLane.RIGHT)
            else:
                self.send_switchlane(SwitchLane.LEFT)
        
        # Throrttle
        if piece_index == self.last_index:
            self.speed = piece_dist - self.last_distance
        else:
            self.pid.setPoint(self.targetSpeed[piece_index])

        self.last_distance = piece_dist
        self.last_index = piece_index

        t = self.pid.update(self.speed)
        t = max(min(t,1),0)

        self.send_throttle(t)
            
    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_lapfinished(self, data):
        print("Lap finished (%d ms)" % data["lapTime"]["millis"])
        self.ping()

    def on_turboavailable(self, data):
        """Turbo is available ALRIGHT."""
        self.ping()
        
    @staticmethod
    def __retrieve_self_carposition_data(car, data):
        for d in data:
            if car == Car(d['id']):
                return d

    @staticmethod
    def __next_switchlane(circuit_pieces, index):
        for n in range(index+1, len(circuit_pieces)):
            if circuit_pieces[n].switch:
                return n
        return -1
