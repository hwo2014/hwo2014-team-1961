class EventHook(object):
    """A nice C# like event system.

    >>> def test(x): print(x)
    >>>
    >>> eventhook = EventHook()
    >>> eventhook += test
    >>> eventhook.fire(2)
    2
    """
    
    def __init__(self):
        self.__listeners = []

    def __iadd__(self, listener):
        self.__listeners.append(listener)
        return self

    def __isub__(self, listener):
        self.__listeners.remove(listener)
        return self

    def fire(self, *args, **keywargs):
        for listener in self.__listeners:
            listener(*args, **keywargs)


if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
