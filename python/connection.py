from eventhook import EventHook

import socket
import sys

class Connection(object):
    """A synchronous wrapper class for a socket, a client can register
    a callback for handling the received messages."""

    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        self.on_receive = EventHook()
    
    def connect(self, address):
        """Connects the client to the specified address."""
        self.socket.connect(address)
        
    def close(self):
        """Disconnects the client."""
        self.socket.close()
    
    def send(self, msg):
        """Sends a message to the server."""
        self.socket.sendall('%s\n' % msg)
        
    def run(self):
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            self.on_receive.fire(line)
            line = socket_file.readline()


if __name__ == '__main__':
    """"Just a test/use case that receives http get data."""
    
    def my_test_callback(received_string):
        print('Received: %s ' % received_string)

    if len(sys.argv) != 3:
        print('Usage: ./connection.py host port')
        sys.exit(-1)

    host, port = sys.argv[1:]

    connection = Connection()
    connection.add_callback(my_test_callback)

    print('Connecting to %s:%s' % (host, port))
    connection.connect((host, int(port)))

    connection.send('GET / HTTP/1.0\n')
    
    connection.run()
    connection.close()
