from circuit import Circuit, Piece, LanePiece


class CircuitGraph(object):
    """A graph represantation of the circuit.

    Two virtual nodes have been added at the beggining and end.
    Example:
    3 pieces (3 lanes): 1 straight line, 1 switch line, 1 straight line.

    ----- | -\ /- | -----
    ----- | --X-- | -----
    ----- | -/ \- | -----

    Produces the following graph:
          (1) - (4) - (7)
        /     X           \ 
    (0) - (2) - (5) - (8) - (10)
        \     X           /
          (3) - (6) - (9)
    """
    
    def __init__(self, vertices, edges):
        self.__vertices = vertices
        self.__edges = edges

    @property
    def vertices(self):
        """A list of the circuit vertices. The first and last one
        are virtual vertices."""
        return self.__vertices

    @property
    def edges(self):
        """A list of index tuples (origin, destination) with the
        connection relationship."""
        return self.__edges
    
    @staticmethod
    def from_circuit(circuit):
        """Produces a graph from the given Circuit object."""
        lanes_count = circuit.lanes_count

        # Adding all vertices, plus two virtual vertices at
        # the beginning and end.
        vertices = [LanePiece.create_dummy()]
        vertices.extend([piecelane for piece in circuit.pieces for piecelane in piece.lanes])
        vertices.append(LanePiece.create_dummy())

        vertices_count = len(vertices)
        
        # Calculating edges.
        # Connect to first virtual vertex.
        edges = {0: [n for n in range(1, lanes_count+1)]}

        previous_vertices = circuit.pieces[0].lanes
        for piece in circuit.pieces[1:]:
            if piece.switch:
                for vertex in previous_vertices:
                    possible_lanes = set([vertex.lane,                            # Current lane
                                          max(vertex.lane - 1, 0),                # Current lane - 1, if possible
                                          min(vertex.lane + 1, lanes_count - 1)]) # Current lane + 1, if possible

                    edges[vertices.index(vertex)] = [vertices.index(piece.lanes[n]) for n in possible_lanes]
            else:
                for n in range(lanes_count):
                    edges[vertices.index(previous_vertices[n])] = [vertices.index(piece.lanes[n])]

            previous_vertices = piece.lanes

        # Connect to last vertex point.
        for n in range(vertices_count - lanes_count - 1, vertices_count - 1):
            edges[n] = [vertices_count - 1]

        return CircuitGraph(vertices, edges)


def find_all_paths(graph):
    """Finds all paths for the given circuit graph."""

    DESTINATION = len(graph.vertices) - 1
    
    def recursive_find(graph, visited, paths=[]):
        # Peek last visited node
        last_visited = visited[-1]

        # Iterate on the current vertex connections
        for node in graph.edges[last_visited]:
            visited.append(node)

            # If current node is destination copy the 
            if node == DESTINATION:
                paths.append(list(visited))
            else:
                recursive_find(graph, visited, paths)

            visited.pop()

        return paths

    return recursive_find(graph, [0])

def sort_lane_paths(circuit):
    """Sorts the all possible paths by their respective track length."""
    
    graph = CircuitGraph.from_circuit(circuit)
    paths = [p[1:-1] for p in find_all_paths(graph)]

    def length_sort(path):
        acc = 0
        for p in path:
            acc += graph.vertices[p].length
        return acc

    def switches_sort(path):
        switches = 0
        previous = graph.vertices[path[0]].lane
        
        for p in path[1:]:
            current = graph.vertices[p].lane
            if current != previous:
                switches += 1
            previous = current

        return switches

    # I'm so glad sorted is stable.
    sorted_paths = sorted(sorted(paths, key=switches_sort), key=length_sort)

    sorted_lanes = []
    for path in sorted_paths:
        sorted_lanes.append([graph.vertices[p].lane for p in path])

    return sorted_lanes
    
def test_graph():
    """Test for CircuitGraph."""

    d = {
        'pieces': [
            {u'length': 100.0},
            {u'length': 100.0,
             'switch': True},
            {u'length': 100.0},
        ],
        'lanes': [
            {
              "distanceFromCenter": -20,
              "index": 0
            },
            {
              "distanceFromCenter": 0,
              "index": 1
            },
            {
              "distanceFromCenter": 20,
              "index": 2
            }
        ]
    }

    expected_vertices_count = 11
    expected_edges = {
        0: [1, 2, 3],
        1: [4, 5],
        2: [4, 5, 6],
        3: [5, 6],
        4: [7],
        5: [8],
        6: [9],
        7: [10],
        8: [10],
        9: [10],
    }
    graph = CircuitGraph.from_circuit(Circuit.from_dict(d))

    assert len(graph.vertices) is expected_vertices_count, \
           'Vertices count does not match.'
    assert graph.edges == expected_edges, \
           'Test edges do not match.'

def test_find_all_paths():
    """Test for find_all_paths."""

    d = {
        'pieces': [
            {u'length': 100.0},
            {u'length': 100.0,
             'switch': True},
            {u'angle': 90.0,
             u'radius': 200},
        ],
        'lanes': [
            {
              "distanceFromCenter": -20,
              "index": 0
            },
            {
              "distanceFromCenter": 0,
              "index": 1
            },
            {
              "distanceFromCenter": 20,
              "index": 2
            }
        ]
    }

    expected_paths = [[0, 1, 4, 7, 10],
                      [0, 1, 5, 8, 10],
                      [0, 2, 4, 7, 10],
                      [0, 2, 5, 8, 10],
                      [0, 2, 6, 9, 10],
                      [0, 3, 5, 8, 10],
                      [0, 3, 6, 9, 10]]

    graph = CircuitGraph.from_circuit(Circuit.from_dict(d))
    bs = find_all_paths(graph)

    assert bs == expected_paths, \
           'Found path does not match the expected one.'

def test_sort_lane_paths():
    """Test for sort_lane_paths."""

    d = {
        'pieces': [
            {u'length': 100.0},
            {u'length': 100.0,
             'switch': True},
            {u'angle': 90.0,
             u'radius': 200},
        ],
        'lanes': [
            {
              "distanceFromCenter": -10,
              "index": 0
            },
            {
              "distanceFromCenter": 10,
              "index": 1
            }
        ]
    }

    expected_lanes = [[1, 1, 1],
                      [0, 1, 1],
                      [0, 0, 0],
                      [1, 0, 0]]
                      
    circuit = Circuit.from_dict(d)
    
    assert sort_lane_paths(circuit) == expected_lanes, \
           'Found lanes do not match the expected ones.'
    
    
if __name__ == '__main__':
    test_graph()
    test_find_all_paths()
    test_sort_lane_paths()

    
