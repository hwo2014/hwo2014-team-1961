import math

class Circuit(object):
    
    def __init__(self, pieces):
        self.__pieces = pieces

    @property
    def pieces(self):
        return self.__pieces

    @property
    def lanes_count(self):
        return self.pieces[0].lanes_count
    
    @staticmethod
    def from_dict(d):
        lanes = [lane for lane in d['lanes']]
        pieces = [Piece.from_dict(piece, lanes) for piece in d['pieces']]

        return Circuit(pieces)


class Piece(object):

    def __init__(self, lanes):
        self.__lanes = lanes

    @property
    def lanes(self):
        return self.__lanes

    @property
    def lanes_count(self):
        """Amount of lanes on the given circuit piece."""
        return len(self.lanes)

    @property
    def switch(self):
        """Checks if the lane contains a switch."""
        return any([lane.switch for lane in self.lanes])

    def angle(self):
        return self.lanes[0].angle

    def radius(self):
        return self.lanes[0].radius

    def length(self, i):
        return self.lanes[i].length

    @staticmethod
    def from_dict(d, lanes):
        lanes = [LanePiece.from_dict(d, lane) for lane in lanes]

        return Piece(lanes)


class LanePiece(object):
    """Specific lane information on a given piece.
    
    >>> lane = LanePiece.from_dict({'length': 100}, {'distanceFromCenter': 0, 'index': 0})
    >>> lane.length
    100
    >>> lane.switch
    False
    >>> lane.angle
    0
    >>> lane.radius
    0
    >>>
    >>> lane = LanePiece.from_dict({'length': 100, 'switch': True}, {'distanceFromCenter': 0, 'index': 0})
    >>> lane.length
    100
    >>> lane.switch
    True
    >>> lane.angle
    0
    >>> lane.radius
    0
    >>>
    >>> lane = LanePiece.from_dict({'radius': 200, 'angle': 22.5}, {'distanceFromCenter': 0, 'index': 0})
    >>> lane.length
    78.53981633974483
    >>> lane.switch
    False
    >>> lane.angle
    22.5
    >>> lane.radius
    200
    """

    def __init__(self, length, switch, angle, radius, lane):
        self.__length = length
        self.__switch = switch
        self.__angle = angle
        self.__radius = radius
        self.__lane = lane

    @property
    def length(self):
        return self.__length
    
    @property
    def switch(self):
        return self.__switch

    @property
    def angle(self):
        return self.__angle

    @property
    def radius(self):
        return self.__radius

    @property
    def lane(self):
        return self.__lane
    
    @staticmethod
    def from_dict(d, lane):
        angle = d.get('angle', 0)
        switch = d.get('switch', False)
        
        if 'radius' in d:
            radius = d['radius'] - lane['distanceFromCenter'] * math.copysign(1, angle)
        else:
            radius = 0
        
        if 'length' in d:
            length = d['length']
        else:
            length = radius * abs(math.radians(angle))
        
        return LanePiece(length, switch, angle, radius, lane['index'])

    @staticmethod
    def create_dummy():
        return LanePiece(0, False, 0, 0, 0)

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
